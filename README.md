# HW SQL


## Задание
1. Create DB
1. Wrire SQL 


## Create env
```BASH
pip install -r requirements.txt   
```

## Start local DB
```BASH
docker compose -f "docker-compose.yml" up -d --build  
```

## Create table in DB
```BASH
docker container exec -it -w /app/sql postgres_local_db psql -U postgres_user -d postgres_db -a -f model.sql
```